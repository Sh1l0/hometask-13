'use strict';
function addUserFetch(userInfo) {
  if(userInfo.id) {
    console.log(new Error(`Был передан id`));
    return;
  }
  if(typeof(userInfo) !== 'object') {
    console.log(new Error(`Был передан не объект`));
    return;
  }
  return fetch('/users', {
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(userInfo)
      }).then(res => {
        if(res.status <= 200 || res.status >= 300) {
          return Promise.reject(new Error(`Код: ${res.status}, ${res.statusText}`));
        }
        return res.json()
      }).then(val => {
        return val.id;
      }).catch(error => {
        console.log(error.message);
      });

  // напишите POST-запрос используя метод fetch
}

function getUserFetch(id) {
  if(userInfo.id) {
    console.log(new Error(`Был передан id`));
    return;
  }
  if(typeof(userInfo) !== 'object') {
    console.log(new Error(`Был передан не объект`));
    return;
  }
  return fetch(`/users/${id}`).then(res => {
    if(res.status >= 200 && res.status < 300) {
      return res.json();
    }
    return Promise.reject(new Error(`Код: ${res.status}, ${res.statusText}`));
  }).catch(error => {
    console.log(error);
  })
  // напишите GET-запрос используя метод fetch
}

function addUserXHR(userInfo) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if(xhr.readyState !== 4) {
        return;
      }
      if(xhr.status >= 200 && xhr.status < 300) {
        resolve(JSON.parse(xhr.responseText).id)
      }
      else {
        reject(new Error(`Код: ${xhr.status}, ${xhr.statusText}`));
      }
    }

    xhr.open('POST', '/users');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(userInfo));

  }).catch(error => {
    console.log(error.message);
  });
  // напишите POST-запрос используя XMLHttpRequest
}

function getUserXHR(id) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if(xhr.readyState !== 4) {
        return;
      }
      if(xhr.status >= 200 && xhr.status < 300) {
        resolve(JSON.parse(xhr.responseText))
      }
      else {
        reject(new Error(`Код: ${xhr.status}, ${xhr.statusText}`));
      }
    }

    xhr.open('GET', `/users/${id}`);
    xhr.send();

  }).catch(error => {
    console.log(error.message);
  });
  // напишите GET-запрос используя XMLHttpRequest
}

function checkWork() {
  addUserXHR({ name: "Alice", lastname: "XMLHttpRequest"})
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

 checkWork();
